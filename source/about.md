---
navigation:
  weight: 70
  title:  About
  hidden: false
title: About
---
# About

Static site produced by [Middleman](https://middlemanapp.com/) from [source on Bitbuket](https://bitbucket.org/thunderrabbit/robnugen-middleman-blog)


